# README #

Dark theme for midnight commander (MC).

![screenshot.png](https://bitbucket.org/repo/LB6aqX/images/3755727498-screenshot.png)

### How do I get set up? ###

* Copy darkskin.ini to /usr/share/mc/skins
* Close MC
* Edit ~/.config/mc/ini: skin=darkskin

### Who do I talk to? ###

* All4DK